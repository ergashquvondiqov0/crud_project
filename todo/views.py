from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse, get_object_or_404
from .models import User
from .forms import StudentRegistration


# Create your views here.

def add_show(request):
    if request.method == 'POST':
        fm = StudentRegistration(request.POST)
        if fm.is_valid():
            firstname = fm.cleaned_data['firstname']
            lastname = fm.cleaned_data['lastname']
            course = fm.cleaned_data['course']
            direction = fm.cleaned_data['direction']
            teacher = fm.cleaned_data['teacher']
            reg = User(firstname=firstname, lastname=lastname, course=course, direction=direction, teacher=teacher)
            reg.save()
            fm = StudentRegistration()
    else:
        fm = StudentRegistration()
    stud = User.objects.all()
    return render(request, 'addandshow.html', {'form': fm, 'stu': stud})


def update_data(request, id):

    if request.method == 'POST':
        pi = User.objects.get(pk=id)
        fm = StudentRegistration(request.POST, instance=pi)
        if fm.is_valid():
            fm.save()
    else:
        pi = User.objects.get(pk=id)
        fm = StudentRegistration(instance=pi)
    return render(request, 'updateandshow.html', {'form': fm})


def delete_data(request, id):
    context = {}
    obj = get_object_or_404(User, id=id)
    if request.method == 'POST':
        # pi = User.objects.get(pk=id)
        # fm = StudentRegistration.get(pk=pi)
        obj.delete()
        return HttpResponseRedirect("/")
    return render(request, "delete_view.html", context)

