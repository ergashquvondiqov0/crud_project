from django.db import models


# Create your models here.

class User(models.Model):
    objects = None
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    course = models.PositiveIntegerField()
    direction = models.CharField(max_length=100)
    teacher = models.CharField(max_length=100)
